require 'csv'

module ImportableConcern
  extend ActiveSupport::Concern

  included do
    def self.import(csv_path)
      CSV.foreach(csv_path, headers: true) do |row, csv|
        updating_attributes = {}

        row.headers.each do |header|
          record = self.find_by(reference: row['reference'])

          # Skip if no record was found
          next if record.blank?

          # Check if the given header is part of the attributes
          # and not id and the attribute is not protected
          if self.attribute_names.include?(header) && header != 'id' &&
            (!self::PROTECTED_ATTRIBUTES.include?(header) || record.attributes[header].blank?)
            updating_attributes[header] = row[header]
          end

          # Do not trigger validation or callback to make the process faster
          record.update_columns(updating_attributes)
        end
      end
    end
  end
end
