class Person < ApplicationRecord
  include ImportableConcern

  PROTECTED_ATTRIBUTES = [ 'email', 'mobile_phone_number', 'home_phone_number', 'address' ]
end
