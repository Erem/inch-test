class Building < ApplicationRecord
  include ImportableConcern

  PROTECTED_ATTRIBUTES = [ 'manager_name' ]
end
