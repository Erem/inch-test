class AddIndexToBuildingReference < ActiveRecord::Migration[5.0]
  def change
    add_index :buildings, :reference, unique: true
  end
end
