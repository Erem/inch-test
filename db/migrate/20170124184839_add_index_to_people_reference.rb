class AddIndexToPeopleReference < ActiveRecord::Migration[5.0]
  def change
    add_index :people, :reference, unique: true
  end
end
