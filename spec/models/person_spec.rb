require 'rails_helper'

RSpec.describe Person, type: :model do
  describe '.import' do
    it 'does not overwrite a protected attribute' do
      person = FactoryGirl.create(:person, reference: 1, email: 'foo@bar.baz')

      Person.import(Rails.root.join('spec', 'factories', 'files', 'people.csv'))

      expect(person.reload.email).to eq('foo@bar.baz')
    end

    it 'overwrites non protected attributes' do
      person = FactoryGirl.create(:person, reference: 1, firstname: 'Foo')

      Person.import(Rails.root.join('spec', 'factories', 'files', 'people.csv'))

      expect(person.reload.firstname).to eq('Henri')
    end

    it 'fills in a null protected attribute' do
      person = FactoryGirl.create(:person, reference: 1, email: nil)

      Person.import(Rails.root.join('spec', 'factories', 'files', 'people.csv'))

      expect(person.reload.email).to eq('h.dupont@gmail.com')
    end

    it 'fills in an empty protected attribute' do
      person = FactoryGirl.create(:person, reference: 1, email: '')

      Person.import(Rails.root.join('spec', 'factories', 'files', 'people.csv'))

      expect(person.reload.email).to eq('h.dupont@gmail.com')
    end
  end
end
