require 'rails_helper'

RSpec.describe Building, type: :model do
  require 'rails_helper'

  describe '.import' do
    it 'does not overwrite a protected attribute' do
      building = FactoryGirl.create(:building, reference: 1, manager_name: 'Mael')

      Building.import(Rails.root.join('spec', 'factories', 'files', 'buildings.csv'))

      expect(building.reload.manager_name).to eq('Mael')
    end

    it 'overwrites non protected attributes' do
      building = FactoryGirl.create(:building, reference: 1, address: '6 rue de la sabliere')

      Building.import(Rails.root.join('spec', 'factories', 'files', 'buildings.csv'))

      expect(building.reload.address).to eq('10 Rue La bruyère')
    end

    it 'fills in a null protected attribute' do
      person = FactoryGirl.create(:building, reference: 1, manager_name: nil)

      Building.import(Rails.root.join('spec', 'factories', 'files', 'buildings.csv'))

      expect(person.reload.manager_name).to eq('Martin Faure')
    end

    it 'fills in an empty protected attribute' do
      person = FactoryGirl.create(:building, reference: 1, manager_name: '')

      Building.import(Rails.root.join('spec', 'factories', 'files', 'buildings.csv'))

      expect(person.reload.manager_name).to eq('Martin Faure')
    end
  end
end
